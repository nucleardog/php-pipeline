<?php

declare(strict_types=1);

namespace Nucleardog\Pipeline\Contracts;

interface UsesConfiguration
{
	public function setConfiguration(array $config): void;
	public function getConfigurationSchema(): array;
}