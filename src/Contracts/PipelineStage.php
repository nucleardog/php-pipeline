<?php

declare(strict_types=1);

namespace Nucleardog\Pipeline\Contracts;
use Illuminate\Support\Collection;

interface PipelineStage
{
	public function __invoke(Collection $items): Collection;

}