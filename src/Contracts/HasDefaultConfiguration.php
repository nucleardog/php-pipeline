<?php

declare(strict_types=1);

namespace Nucleardog\Pipeline\Contracts;

interface HasDefaultConfiguration
{
	public function getDefaultConfiguration(): array;
}