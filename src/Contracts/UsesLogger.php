<?php

declare(strict_types=1);

namespace Nucleardog\Pipeline\Contracts;
use Psr\Log\LoggerInterface;

interface UsesLogger
{
	public function setLogger(LoggerInterface $logger): void;
}