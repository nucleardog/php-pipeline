<?php

declare(strict_types=1);

namespace Nucleardog\Pipeline;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Psr\Log\LoggerTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

abstract class Stage implements Contracts\PipelineStage, Contracts\HasDefaultConfiguration, Contracts\UsesConfiguration, Contracts\UsesLogger, LoggerInterface
{
	use LoggerTrait;

	private array $config;
	private LoggerInterface $logger;

	public abstract function getConfigurationSchema(): array;
	public abstract function __invoke(Collection $input): Collection;

	public function getDefaultConfiguration(): array
	{
		return [];
	}

	public function setConfiguration(array $config): void
	{
		$this->config = $config;
	}

	public function setLogger(LoggerInterface $logger): void
	{
		$this->logger = $logger;
	}

	protected function config(string $key): mixed
	{
		return Arr::get($this->config, $key);
	}

	public function abort(?string $message = null): void
	{
		throw new Exceptions\PipelineAbortedException($message ?? '', stage: $this);
	}

	public function error(string|\Stringable $message, array $context = []): void
	{
		$this->log(LogLevel::ERROR, $message, $context);
		throw new Exceptions\PipelineStageException((string)$message, stage: $this);
	}

	public function critical(string|\Stringable $message, array $context = []): void
	{
		$this->log(LogLevel::CRITICAL, $message, $context);
		throw new Exceptions\PipelineStageException((string)$message, stage: $this);
	}

	public function alert(string|\Stringable $message, array $context = []): void
	{
		$this->log(LogLevel::ALERT, $message, $context);
		throw new Exceptions\PipelineStageException((string)$message, stage: $this);
	}

	public function emergency(string|\Stringable $message, array $context = []): void
	{
		$this->log(LogLevel::EMERGENCY, $message, $context);
		throw new Exceptions\PipelineStageException((string)$message, stage: $this);
	}

    public function log($level, string|\Stringable $message, array $context = []): void
    {
        $this->logger->log($level, $message, $context);
    }

}