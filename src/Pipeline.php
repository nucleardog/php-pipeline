<?php

declare(strict_types=1);

namespace Nucleardog\Pipeline;

use \Illuminate\Support\Collection;
use Psr\Log\LoggerInterface;

class Pipeline
{
	private array $stages;

	public function __construct(
		private readonly LoggerInterface $logger = new NullLogger(),
		private array $config = [],
	) {
		$this->stages = [];
	}

	public function add(Contracts\PipelineStage|\Closure $stage, array $config = []): static
	{

		if ($stage instanceof \Closure)
		{
			$callback = $stage;
			$stage = new class($callback) extends Stage {
				public function __construct(private \Closure $callback) {}
				public function getConfigurationSchema(): array { return []; }
				public function __invoke(Collection $items): Collection { return $this->callback->bindTo($this, $this)->__invoke($items); }
			};
		}

		if ($stage instanceof Contracts\UsesLogger)
			$stage->setLogger($this->logger);

		if ($stage instanceof Contracts\UsesConfiguration)
		{
			$resolved_config = [];

			if ($stage instanceof Contracts\HasDefaultConfiguration)
				$resolved_config = $stage->getDefaultConfiguration();

			$resolved_config = array_merge($resolved_config, $this->config);
			$resolved_config = array_merge($resolved_config, $config);

			if (class_exists('Illuminate\\Validation\\Validator'))
			{
				// @phan-suppress-next-line PhanUndeclaredClassMethod
				$validator = $this->getValidatorFactory()->make($resolved_config, $stage->getConfigurationSchema());
				$validator->validate();
			}

			$stage->setConfiguration($resolved_config);
		}

		$this->stages[] = $stage;

		return $this;
	}

	public function __invoke(Collection $items): Collection
	{
		foreach ($this->stages as $stage)
		{
			$items = $stage->__invoke($items);
		}
		return $items;
	}

	/**
 	 * @phan-suppress PhanUndeclaredClassMethod, PhanUndeclaredTypeReturnType
 	 */
	private function getValidatorFactory(): \Illuminate\Validation\Factory
	{
		static $factory = null;
		if (empty($factory))
		{
			$loader = new \Illuminate\Translation\ArrayLoader();
			$translator = new \Illuminate\Translation\Translator($loader, 'en_US');
			$factory = new \Illuminate\Validation\Factory($translator);
		}
		return $factory;
	}

}