<?php

declare(strict_types=1);

namespace Nucleardog\Pipeline\Exceptions;

class PipelineException extends \Exception
{
}