<?php

declare(strict_types=1);

namespace Nucleardog\Pipeline\Exceptions;
use Nucleardog\Pipeline\Contracts\PipelineStage;

class PipelineStageException extends PipelineException
{
	protected readonly ?PipelineStage $stage;

	public function __construct(string $message = '', int $code = 0, ?\Throwable $previous = null, ?PipelineStage $stage = null)
	{
		parent::__construct($message, $code, $previous);
		$this->stage = $stage;
	}

	public function getStage(): ?PipelineStage
	{
		return $this->stage;
	}

}