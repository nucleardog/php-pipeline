<?php

declare(strict_types=1);

namespace Nucleardog\Pipeline;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

class NullLogger implements LoggerInterface
{
	use LoggerTrait;

	/**
 	 * @phan-suppress PhanUnusedPublicMethodParameter
 	 */
    public function log($level, string|\Stringable $message, array $context = []): void
    {
    }
}