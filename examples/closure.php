<?php

require(__DIR__.'/common.php');

$pipeline = new \Nucleardog\Pipeline\Pipeline(new StreamLogger(fopen('php://stdout', 'w')), []);

$pipeline->add(function($items) {
	return $items->merge(['a' => true, 'b' => true, 'c' => false, 'd' => true]);
});

$pipeline->add(function($items) {
	return $items->reject(fn($item) => $item === false);
});

$c = new \Illuminate\Support\Collection();
$c = $pipeline($c);

var_dump($c);
