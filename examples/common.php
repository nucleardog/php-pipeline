<?php

require(__DIR__.'/../vendor/autoload.php');

class StreamLogger implements \Psr\Log\LoggerInterface
{
	use \Psr\Log\LoggerTrait;

	public function __construct(
		private $stream
	) {
	}

	public function log($level, string|\Stringable $message, array $context = []): void
	{
		fputs($this->stream, sprintf('[%-12s] %s %s', strtoupper($level), $message, empty($context) ? '' : json_encode($context)).PHP_EOL);
	}
}